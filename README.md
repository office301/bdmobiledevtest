# Prerequisities
## BDMobileDevTest SPA
* [Git](http://git-scm.com/) or [Source Tree](https://www.sourcetreeapp.com/) to manage sources - see the installation instructions on the site
* [Node.js](http://nodejs.org/) - see the installation instructions on the site
* [Bower](http://bower.io/): We use Bower to manage client-side packages for the docs. Install the bower command-line tool globally with:
```
npm install -g bower
```
* [Ionic CLI](http://ionicframework.com/docs/cli/install.html) - see the installation instructions on the site

# Quick Start
## BDMobileDevTest SPA
* clone the source code repository
* go to project repository directory (<Git>\src/BDMobileDevTest.App)
* install node modules using npm
```
npm install
```
* install bower components using bower
```
bower install
```

# Configuring 
## BDMobileDevTest SPA
- go to <Git>\src\BDMobileDevTest.App\www\js directory
- set the below params in config.js file:
  * serverURL

# Running
## BDMobileDevTest SPA
### Running from console
* go to project repository directory (<Git>\src\BDMobileDevTest.App)
* launch a http server and serve BDMobileDevTest SPA at http://localhost:8100/
```
ionic serve
```
### Editing in Visual Studio 2015
* go to main project repository directory (<Git>) and open Visual Studio 2015 solution file **BDMobileDevTest.sln**

# Deploying
## BDMobileDevTest SPA
Before preparing distribution package please make the necessary configuration changes as described in ** Configuring ** section above
* go to project repository directory (<Git>\src\BDMobileDevTest.App)
* build by issuing the below command
```
ionic build android
```
* or, run by issuing the below command
```
ionic run android
```

# Running tests
## BDMobileDevTest SPA
To execute all unit tests
* go to project repository directory (<Git>\src\BDMobileDevTest.App)
* issue the below command
```
npm test
```
In the window opened click Debug. To re-run the test please refresh this browser window

# Pushing New Release
## BDMobileDevTest SPA
TODO: Write instructions to push new releases

# Software stack
See **bower.json** in **<Git>\src\BDMobileDevTest.App\** directory

# History
1. v0.0.1