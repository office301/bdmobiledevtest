﻿(function() {
    'use strict';

    describe('BarcodeCtrl', function() {

        var noop = function() {
        };
        var noopPromise = function() {
            var deferred = $q.defer();
            setTimeout(
            function() {
                deferred.resolve();
            }, 100);
            return deferred.promise;
        };

        var BarcodeCtrl, $controller, $scope, $rootScope, $timeout, $q;

        beforeEach(module('starter.config'));
        beforeEach(module('starter.services'));
        beforeEach(module('starter.controllers'));

        beforeEach(inject(function(_$controller_, _$rootScope_, _$timeout_, _$q_) {
            $controller = _$controller_;
            $rootScope = _$rootScope_;

            $scope = $rootScope.$new();

            $timeout = _$timeout_;
            $q = _$q_;

            window.cordova = {
                plugins: {
                    barcodeScanner: {
                        scan: noopPromise
                    }
                }
            }
        }));

        it('should invoke scan and correctly reset scope params', function() {
            BarcodeCtrl = $controller('BarcodeCtrl', {
                $scope, $rootScope, $timeout
            });
            $scope.scan();

            expect($scope.done).toBeFalsy();
            expect($scope.success).toBeUndefined();
            expect($scope.result).toBeUndefined();
        });

        it('should invoke scan and correctly handle successful scan result', function(done) {

            spyOn(cordova.plugins.barcodeScanner, 'scan').and.callFake(function() {
                var deferred = $q.defer();
                $timeout(function() {
                    var result = {
                        text: 'text',
                        format: 'format',
                        cancelled: false
                    };
                    deferred.resolve(result);
                }, 100);

                return deferred.promise;
            });

            BarcodeCtrl = $controller('BarcodeCtrl', {
                $scope, $rootScope, $timeout
            });
            $scope.scan();
            $timeout.flush(100);
            done();

            expect(cordova.plugins.barcodeScanner.scan).toHaveBeenCalled();
            expect($scope.done).toBeTruthy();
            expect($scope.success).toBeTruthy();
            expect($scope.result).toEqual("Result: text; Format: format; Cancelled: false");
        });

        it('should invoke scan and correctly handle failed scan result', function(done) {

            spyOn(cordova.plugins.barcodeScanner, 'scan').and.callFake(function(done) {
                var deferred = $q.defer();
                $timeout(function() {
                    var error = 'error message';
                    deferred.reject(error);
                }, 100);

                return deferred.promise;
            });

            BarcodeCtrl = $controller('BarcodeCtrl', {
                $scope, $rootScope, $timeout
            });
            $scope.scan();
            done();

            expect(cordova.plugins.barcodeScanner.scan).toHaveBeenCalled();
            expect($scope.done).toBeFalsy();
            expect($scope.success).not.toBeUndefined();
            expect($scope.success).toBeFalsy();
            expect($scope.result).toBeUndefined();
        });

    });

})();