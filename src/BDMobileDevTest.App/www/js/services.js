﻿(function() {
    'use strict';

    angular.module('starter.services', ['starter.config'])

      // service to proxy to RESTful service
      .factory('ServerProxyService', ['$rootScope', '$log', '$http', '$q', '$timeout', 'SETTINGS', function($rootScope, $log, $http, $q, $timeout, SETTINGS) {

          $http.defaults.headers.common['Authorization'] = 'Token token=2dffc2d32fa614ad680ced119d681d5a16214cc87e33b447cb';

          var self = this;

          self.timeoutInMilliseconds = 120000; //15000;
          self.mockMethodsDelayMilliseconds = 2000;

          self.invokeGetMethod = function(methodName, methodParams) {
              var deferred = $q.defer();
              $timeout(function() {
                  deferred.resolve(); // this aborts the request!
              }, self.timeoutInMilliseconds);

              var _methodParams = $.extend({}, { '_t': moment().valueOf() }, methodParams);
              return $http.get(SETTINGS.serverURL + methodName, { params: _methodParams, timeout: deferred.promise }).
                success(function(data, status, headers, config) {
                    $log.debug('ServerProxy: invokeGetMethod was successfully executed with params - methodName: ' + methodName + ' and params: ' + JSON.stringify(_methodParams));
                    return data;
                }).
                error(function(data, status, headers, config) {
                    if(status === 0) {
                        // $http timeout
                        $log.warn('ServerProxy: timeout error occurs when invokeGetMethod was executed with params - methodName: ' + methodName + ' and params: ' + JSON.stringify(_methodParams));
                    } else {
                        // response error status from server
                        $log.warn('ServerProxy: error occurs when invokeGetMethod was executed with params - - methodName: ' + methodName + ' and params: ' + JSON.stringify(_methodParams) + '. status:' + status);
                    }
                });
          };

          self.invokePostMethod = function(methodName, methodParams) {
              var deferred = $q.defer();
              $timeout(function() {
                  deferred.resolve(); // this aborts the request!
              }, self.timeoutInMilliseconds);

              return $http.post(SETTINGS.serverURL + methodName, methodParams, { timeout: deferred.promise }).
                success(function(data, status, headers, config) {
                    $log.debug('ServerProxy: invokePostMethod was successfully executed with params - methodName: ' + methodName + ' and methodParams: ' + JSON.stringify(methodParams));
                    return data;
                }).
                error(function(data, status, headers, config) {
                    if(status === 0) {
                        // $http timeout
                        $log.warn('ServerProxy: timeout error occurs when invokePostMethod was executed with params - methodName: ' + methodName + ' and methodParams: ' + JSON.stringify(methodParams));
                    } else {
                        // response error status from server
                        $log.warn('ServerProxy: error occurs when invokePostMethod was executed with params - - methodName: ' + methodName + ' and methodParams: ' + JSON.stringify(methodParams) + '. status:' + status);
                    }
                });
          };

          // This method invokes POST method and pass both URI and Body Parameters
          self.invokePostMethoWithURIAndBodyParameters = function(methodName, uriParams, bodyParams) {
              var deferred = $q.defer();
              $timeout(function() {
                  deferred.resolve(); // this aborts the request!
              }, self.timeoutInMilliseconds);

              var uriParamsFormatted = $.param(uriParams);
              return $http.post(SETTINGS.serverURL + methodName + '?' + uriParamsFormatted, bodyParams, { timeout: deferred.promise, headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).
                success(function(data, status, headers, config) {
                    $log.debug('ServerProxy:invokePostMethoWithURIAndBodyParameters was successfully executed with params - methodName: ' + methodName + ' and uriParams: ' + JSON.stringify(uriParams) + ' and bodyParams: ' + JSON.stringify(bodyParams));
                    return data;
                }).
                error(function(data, status, headers, config) {
                    if(status === 0) {
                        // $http timeout
                        $log.warn('ServerProxy: timeout error occurs when invokePostMethoWithURIAndBodyParameters was executed with params - methodName: ' + methodName + ' and uriParams: ' + JSON.stringify(uriParams) + ' and bodyParams: ' + JSON.stringify(bodyParams));
                    } else {
                        // response error status from server
                        $log.warn('ServerProxy: error occurs when invokePostMethoWithURIAndBodyParameters was executed with params - - methodName: ' + methodName + ' and uriParams: ' + JSON.stringify(uriParams) + ' and bodyParams: ' + JSON.stringify(bodyParams) + ' and status:' + status);
                    }
                });
          };

          self.invokeDeleteMethod = function(methodName, methodParams) {
              var deferred = $q.defer();
              $timeout(function() {
                  deferred.resolve(); // this aborts the request!
              }, self.timeoutInMilliseconds);

              var _methodParams = $.extend({}, { '_t': moment().valueOf() }, methodParams);
              return $http.delete(SETTINGS.serverURL + methodName, { params: _methodParams, timeout: deferred.promise }).
                success(function(data, status, headers, config) {
                    $log.debug('ServerProxy: invokeDeleteMethod was successfully executed with params - methodName: ' + methodName + ' and params: ' + JSON.stringify(_methodParams));
                    return data;
                }).
                error(function(data, status, headers, config) {
                    if(status === 0) {
                        // $http timeout
                        $log.warn('ServerProxy: timeout error occurs when invokeDeleteMethod was executed with params - methodName: ' + methodName + ' and params: ' + JSON.stringify(_methodParams));
                    } else {
                        // response error status from server
                        $log.warn('ServerProxy: error occurs when invokeGetMethod was executed with params - - methodName: ' + methodName + ' and params: ' + JSON.stringify(_methodParams) + '. status:' + status);
                    }
                });
          };

          // Method to call REST API method to get list
          // http://first.bossdesk.io/api/v1/cis
          self.getList = function() {
              return self.invokeGetMethod('cis');

              // fake JSON to test UI without communicating with the server
//              var deferred = $q.defer();
//              $timeout(function() {
//                  var _list = [
                    //{
                    //    "id": 17205,
                    //    "name": "ATL2WK12RMT01",
                    //    "alias": "Remote Dev!!!",
                    //    "online": true,
                    //    "updated_at": "2016-07-06T15:17:11.750-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows Server 2012 R2 Standard Evaluation",
                    //            "version": "6.3.9600",
                    //            "service_pack": "0.0"
                    //        }
                    //    },
                    //    "type": {
                    //        "id": 300,
                    //        "name": "Virtual",
                    //        "icon": "fa-bars"
                    //    },
                    //    "status": {
                    //        "id": 73,
                    //        "name": "Active",
                    //        "color": "#34B632"
                    //    }
                    //},
                    //{
                    //    "id": 17462,
                    //    "name": "ATL2WK12SUP05",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-07-05T13:51:03.671-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows Server 2012 R2 Standard Evaluation",
                    //            "version": "6.3.9600",
                    //            "service_pack": "0.0"
                    //        }
                    //    },
                    //    "type": {
                    //        "id": 300,
                    //        "name": "Virtual",
                    //        "icon": "fa-bars"
                    //    },
                    //    "status": {
                    //        "id": 73,
                    //        "name": "Active",
                    //        "color": "#34B632"
                    //    }
                    //},
                    //{
                    //    "id": 17459,
                    //    "name": "ATL2WK16SRTR01",
                    //    "alias": null,
                    //    "online": false,
                    //    "updated_at": "2016-07-05T13:51:03.646-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows Server 2016 Technical Preview 4",
                    //            "version": "10.0.10586",
                    //            "service_pack": "0.0"
                    //        }
                    //    },
                    //    "type": {
                    //        "id": 300,
                    //        "name": "Virtual",
                    //        "icon": "fa-bars"
                    //    },
                    //    "status": {
                    //        "id": 73,
                    //        "name": "Active",
                    //        "color": "#34B632"
                    //    }
                    //},
                    //{
                    //    "id": 17298,
                    //    "name": "ATLENGW2KS",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-07-05T13:51:03.545-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows Server 2003, Enterprise Edition",
                    //            "version": "5.2.3790",
                    //            "service_pack": "2.0"
                    //        }
                    //    },
                    //    "type": {
                    //        "id": 300,
                    //        "name": "Virtual",
                    //        "icon": "fa-bars"
                    //    },
                    //    "status": {
                    //        "id": 73,
                    //        "name": "Active",
                    //        "color": "#34B632"
                    //    }
                    //},
                    //{
                    //    "id": 17209,
                    //    "name": "ATLKRISG",
                    //    "alias": "Kris",
                    //    "online": true,
                    //    "updated_at": "2016-07-05T13:53:10.061-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows 10 Pro",
                    //            "version": "10.0.10586",
                    //            "service_pack": "0.0"
                    //        }
                    //    },
                    //    "type": {
                    //        "id": 299,
                    //        "name": "Laptop",
                    //        "icon": "fa-bars"
                    //    },
                    //    "status": {
                    //        "id": 73,
                    //        "name": "Active",
                    //        "color": "#34B632"
                    //    }
                    //},
                    //{
                    //    "id": 17312,
                    //    "name": "ATLVS0601",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-07-05T13:51:03.570-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows Server 2003, Standard Edition",
                    //            "version": "5.2.3790",
                    //            "service_pack": "2.0"
                    //        }
                    //    },
                    //    "type": {
                    //        "id": 300,
                    //        "name": "Virtual",
                    //        "icon": "fa-bars"
                    //    },
                    //    "status": {
                    //        "id": 73,
                    //        "name": "Active",
                    //        "color": "#34B632"
                    //    }
                    //},
                    //{
                    //    "id": 17326,
                    //    "name": "ATLVS0801",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-07-05T13:51:03.596-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows Server 2003, Standard Edition",
                    //            "version": "5.2.3790",
                    //            "service_pack": "2.0"
                    //        }
                    //    },
                    //    "type": {
                    //        "id": 300,
                    //        "name": "Virtual",
                    //        "icon": "fa-bars"
                    //    },
                    //    "status": {
                    //        "id": 73,
                    //        "name": "Active",
                    //        "color": "#34B632"
                    //    }
                    //},
                    //{
                    //    "id": 17463,
                    //    "name": "ATLW2K12DEV03",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-07-05T13:51:03.697-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows Server 2012 R2 Standard Evaluation",
                    //            "version": "6.3.9600",
                    //            "service_pack": "0.0"
                    //        }
                    //    },
                    //    "type": {
                    //        "id": 300,
                    //        "name": "Virtual",
                    //        "icon": "fa-bars"
                    //    },
                    //    "status": {
                    //        "id": 73,
                    //        "name": "Active",
                    //        "color": "#34B632"
                    //    }
                    //},
                    //{
                    //    "id": 17292,
                    //    "name": "ATLW2K12R2SUP04",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-07-05T13:51:03.517-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows Server 2012 R2 Datacenter",
                    //            "version": "6.3.9600",
                    //            "service_pack": "0.0"
                    //        }
                    //    },
                    //    "type": {
                    //        "id": 300,
                    //        "name": "Virtual",
                    //        "icon": "fa-bars"
                    //    },
                    //    "status": {
                    //        "id": 73,
                    //        "name": "Active",
                    //        "color": "#34B632"
                    //    }
                    //},
                    //{
                    //    "id": 17324,
                    //    "name": "ATLW2K12SUP00",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-07-05T13:51:03.490-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows Server 2012 Standard",
                    //            "version": "6.2.9200",
                    //            "service_pack": "0.0"
                    //        }
                    //    },
                    //    "type": {
                    //        "id": 300,
                    //        "name": "Virtual",
                    //        "icon": "fa-bars"
                    //    },
                    //    "status": {
                    //        "id": 73,
                    //        "name": "Active",
                    //        "color": "#34B632"
                    //    }
                    //},
                    //{
                    //    "id": 17328,
                    //    "name": "ATLW2K8FLASH",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-07-05T13:51:03.622-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows Server 2008 R2 Enterprise",
                    //            "version": "6.1.7601",
                    //            "service_pack": "1.0"
                    //        }
                    //    },
                    //    "type": {
                    //        "id": 300,
                    //        "name": "Virtual",
                    //        "icon": "fa-bars"
                    //    },
                    //    "status": {
                    //        "id": 73,
                    //        "name": "Active",
                    //        "color": "#34B632"
                    //    }
                    //},
                    //{
                    //    "id": 17295,
                    //    "name": "ATLW2K8R2LAB01",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-06-07T09:06:34.322-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows Server 2008 R2 Standard",
                    //            "version": "6.1.7601",
                    //            "service_pack": "1.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 17302,
                    //    "name": "ATLW2K8R2SUP01",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-06-07T09:06:28.176-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows Server 2008 R2 Standard",
                    //            "version": "6.1.7601",
                    //            "service_pack": "1.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 17304,
                    //    "name": "ATLW2K8R2SUP02",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-06-07T09:06:44.645-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows Server 2008 R2 Standard",
                    //            "version": "6.1.7601",
                    //            "service_pack": "1.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 17307,
                    //    "name": "ATLW2K8R2SUP03",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-06-07T09:07:19.922-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows Server 2008 R2 Enterprise",
                    //            "version": "6.1.7601",
                    //            "service_pack": "1.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 17274,
                    //    "name": "ATLW2K8VC",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-06-07T09:06:25.785-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows Server 2008 R2 Standard",
                    //            "version": "6.1.7601",
                    //            "service_pack": "1.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 17306,
                    //    "name": "ATLW7DEV01",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-06-07T09:07:09.299-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows 7 Enterprise",
                    //            "version": "6.1.7601",
                    //            "service_pack": "1.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 17335,
                    //    "name": "ATLW7LT60",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-06-07T09:07:02.027-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows 7 Enterprise N",
                    //            "version": "6.1.7601",
                    //            "service_pack": "1.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 17316,
                    //    "name": "ATLWIN64L115",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-06-06T09:06:37.226-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows 7 Ultimate",
                    //            "version": "6.1.7601",
                    //            "service_pack": "1.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 17311,
                    //    "name": "ATLWXPLAB01",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-06-07T09:05:32.214-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows XP Professional",
                    //            "version": "5.1.2600",
                    //            "service_pack": "3.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 17453,
                    //    "name": "BOSS1214-PC",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-06-06T09:05:59.357-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows 10 Pro",
                    //            "version": "10.0.10586",
                    //            "service_pack": "0.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 17464,
                    //    "name": "BOSS425WIN10-PC",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-06-07T09:07:24.895-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows 10 Enterprise Evaluation",
                    //            "version": "10.0.10586",
                    //            "service_pack": "0.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 17460,
                    //    "name": "BOSSADMIN7TE-PC",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-06-07T09:06:21.904-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows 10 Enterprise Evaluation",
                    //            "version": "10.0.10586",
                    //            "service_pack": "0.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 17294,
                    //    "name": "BOSS-HP",
                    //    "alias": null,
                    //    "online": false,
                    //    "updated_at": "2016-06-06T09:06:53.010-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows 7 Enterprise",
                    //            "version": "6.1.7601",
                    //            "service_pack": "1.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 20109,
                    //    "name": "COMPUTER-1000",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-06-13T21:38:42.649-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Microsoft Windows 10 Pro",
                    //            "version": "10.0.10586",
                    //            "service_pack": "0.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 17313,
                    //    "name": "DEMO",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-06-07T09:06:37.564-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows Server 2008 R2 Standard",
                    //            "version": "6.1.7601",
                    //            "service_pack": "1.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 20229,
                    //    "name": "DESKTOP-DT18MA2",
                    //    "alias": null,
                    //    "online": null,
                    //    "updated_at": "2016-07-05T15:18:51.930-04:00",
                    //    "inventory": {
                    //        "os": null
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 17331,
                    //    "name": "JEFF",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-06-07T09:06:31.006-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows 7 Enterprise",
                    //            "version": "6.1.7601",
                    //            "service_pack": "1.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 17346,
                    //    "name": "JULIE-PC",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-06-06T09:06:13.082-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows 7 Enterprise",
                    //            "version": "6.1.7601",
                    //            "service_pack": "1.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //},
                    //{
                    //    "id": 17337,
                    //    "name": "LELAND-PC",
                    //    "alias": null,
                    //    "online": true,
                    //    "updated_at": "2016-06-07T09:05:33.898-04:00",
                    //    "inventory": {
                    //        "os": {
                    //            "name": "Windows 10 Pro",
                    //            "version": "10.0.10586",
                    //            "service_pack": "0.0"
                    //        }
                    //    },
                    //    "type": null,
                    //    "status": null
                    //}
//                  ];
//                  var response = {
//                      data: _list
//                  };
//                  deferred.resolve(response);
//              }, self.mockMethodsDelayMilliseconds);

//              return deferred.promise;
          };

          return self;
      }])

})();