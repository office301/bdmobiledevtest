angular.module('starter.controllers', ['starter.services', ])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    // Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
        $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function() {
        $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
        console.log('Doing login', $scope.loginData);

        // Simulate a login delay. Remove this and replace with your login
        // code if using a login system
        $timeout(function() {
            $scope.closeLogin();
        }, 1000);
    };
})

.controller('ListCtrl', ['$scope', 'ServerProxyService',
    function($scope, ServerProxyService) {

        $scope.init = function() {

            $scope.list = undefined;
            ServerProxyService.getList()
              .then(function(response) {
                  var _list = response.data;
                  $scope.list = _list;
              });

        }

        $scope.init();
    }
])

.controller('BarcodeCtrl', ['$scope', '$rootScope', '$timeout',
    function($scope, $rootScope, $timeout) {

        $scope.done = false;
        $scope.success = undefined;
        $scope.result = undefined;

        $scope.scan = function() {

            $scope.done = false;
            $scope.success = undefined;
            $scope.result = undefined;

            cordova.plugins.barcodeScanner.scan(
                function(result) {
                    $scope.done = true;
                    $scope.success = true;
                    $scope.result = "Result: " + result.text + "; Format: " + result.format + "; Cancelled: " + result.cancelled;
                    console.info("Scanning success; " + "Result: " + result.text + "; Format: " + result.format + "; Cancelled: " + result.cancelled);
                    $timeout(function() {
                        $rootScope.$digest();
                    });
                },
                function(error) {
                    $scope.done = true;
                    $scope.success = false;
                    $scope.result = error;
                    console.error("Scanning failed: " + error);
                    $timeout(function() {
                        $rootScope.$digest();
                    });
                }
            );
        };

    }
]);
