﻿(function () {
    'use strict';

    angular.module('starter.config', [])
        .constant('SETTINGS', {
            serverURL: 'http://first.bossdesk.io/api/v1/'
        });

})();